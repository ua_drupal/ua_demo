# UA Demo Content *(now deprecated: use [UAQS Demo](https://bitbucket.org/ua_drupal/uaqs_demo) instead)*

Provides some sample content and menu links to demonstrate various features of the [UA Zen theme](https://bitbucket.org/uabrandingdigitalassets/ua_zen) and other UA Drupal components/features.

## Features

- Provides sample Basic Page nodes.
- Provides sample Featured Content nodes.
- Provides sample menu links.
- Sample content/links automatically created when module is enabled.
- Sample content/links automatically removed when the module is disabled.
- More to come...
